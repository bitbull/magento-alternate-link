<?php
/**
 * Class     Observer.php
 * @category Bitbull
 * @package  Bitbull_AlternateLink
 * @author   Mirko Cesaro <mirko.cesaro@bitbull.it>
 */

class Bitbull_AlternateLink_Model_Observer {

    public function alternateLinks()
    {
        $headBlock = Mage::app()->getLayout()->getBlock('head');
        if (!$headBlock) {
            return $this;
        }

        $stores = Mage::app()->getStores();
        $currentStoreId = Mage::app()->getStore()->getId();

        foreach ($stores as $store) {
            // Skip same store and disabled stores
            if ($store->getId() == $currentStoreId || !$store->getIsActive()) {
                continue;
            }
            
            //we use the rewrite inside this module to convert the url
            $url = $store->getCurrentUrl(true, true);

            $storeCode = substr(Mage::getStoreConfig('general/locale/code', $store->getId()),0,2);
            $prviousRelParam = Mage::helper('bitbull_alternatelink')->getRelParamsOfLinkInHead($url, $headBlock);

            if(strlen($prviousRelParam)==0){
                //only if there is no link inside the head we can add the alternate link
                $headBlock->addLinkRel(' alternate"' . ' hreflang="' . $storeCode, $url);
            }

        }
        
        return $this;
    }
} 