<?php
/**
 * Class     Store.php
 * @category Bitbull
 * @package  Bitbull_AlternateLink
 * @author   Mirko Cesaro <mirko.cesaro@bitbul.it>
 */

class Bitbull_AlternateLink_Model_Core_Store extends Mage_Core_Model_Store{

    public function getCurrentUrl($fromStore = true, $removeSessionIdQuery = false)
    {
        $sidQueryParam = $this->_getSession()->getSessionIdQueryParam();
        $requestString = Mage::getSingleton('core/url')->escape(
            ltrim(Mage::app()->getRequest()->getRequestString(), '/'));

        $requestString = $this->_removeFilterInRequestString($requestString);

        $storeUrl = Mage::app()->getStore()->isCurrentlySecure()
            ? $this->getUrl('', array('_secure' => true))
            : $this->getUrl('');
        $storeParsedUrl = parse_url($storeUrl);

        $storeParsedQuery = array();
        if (isset($storeParsedUrl['query'])) {
            parse_str($storeParsedUrl['query'], $storeParsedQuery);
        }

        $currQuery = Mage::app()->getRequest()->getQuery();
        if (isset($currQuery[$sidQueryParam]) && !empty($currQuery[$sidQueryParam])
            && $this->_getSession()->getSessionIdForHost($storeUrl) != $currQuery[$sidQueryParam]
        ) {
            unset($currQuery[$sidQueryParam]);
        }

        foreach ($currQuery as $k => $v) {
            $storeParsedQuery[$k] = $v;
        }

        if (!Mage::getStoreConfigFlag(Mage_Core_Model_Store::XML_PATH_STORE_IN_URL, $this->getCode())) {
            $storeParsedQuery['___store'] = $this->getCode();
        }
        if ($fromStore !== false) {
            $storeParsedQuery['___from_store'] = $fromStore === true ? Mage::app()->getStore()->getCode() : $fromStore;
        }

        if(
            isset($storeParsedQuery['___from_store']) &&
            (
                //remove store info if the target store has the storename in the path
                Mage::getStoreConfigFlag(
                    Mage_Core_Model_Store::XML_PATH_STORE_IN_URL,$storeParsedQuery['___from_store']
                )
                //or the store are the same
                ||
                $this->getCode() == $storeParsedQuery['___from_store']
                //or the base url different
                ||
                $this->_isBaseUrlDifferentByCurrentStore()

            )
        ){
            if(strlen($requestString)>0 && $this->getCode() != $storeParsedQuery['___from_store']){
                $resultConvertionRequest = $this->_convertUrl($requestString);
                if(strlen($resultConvertionRequest)>0){
                    $requestString = $resultConvertionRequest;
                }
            }
            unset($storeParsedQuery['___store']);
            unset($storeParsedQuery['___from_store']);
        }

        if($removeSessionIdQuery){
            unset($storeParsedQuery[$sidQueryParam]);
        }

        return $storeParsedUrl['scheme'] . '://' . $storeParsedUrl['host']
        . (isset($storeParsedUrl['port']) ? ':' . $storeParsedUrl['port'] : '')
        . $storeParsedUrl['path'] . $requestString
        . ($storeParsedQuery ? '?'.http_build_query($storeParsedQuery, '', '&amp;') : '');
    }

    protected function _isBaseUrlDifferentByCurrentStore(){
        return
            Mage::getStoreConfig(Mage_Core_Model_Store::XML_PATH_UNSECURE_BASE_URL)
            !=
            Mage::getStoreConfig(Mage_Core_Model_Store::XML_PATH_UNSECURE_BASE_URL, $this->getCode());
    }

    protected function _convertUrl($requestString)
    {
        $currentStore = Mage::app()->getStore()->getId();
        /** @var Mage_Core_Model_Url_Rewrite $rewrite */
        $rewrite = Mage::getModel('core/url_rewrite');

        //calculate the rewrite using request path of current store
        $rewrite->setStoreId($currentStore);
        $rewrite->loadByRequestPath($requestString);

        //calculate the rewrite using the calculated id path and the destination store
        $rewrite->setStoreId($this->getId());
        $rewrite->loadByIdPath($rewrite->getIdPath());

        return $rewrite->getRequestPath();
    }

    protected function _removeFilterInRequestString($requestString){
        if(strlen($requestString) && strpos($requestString,'/filter/') !== false){
            $requestString = substr($requestString, 0, strpos($requestString, '/filter/'));
        }
        return $requestString;
    }

} 