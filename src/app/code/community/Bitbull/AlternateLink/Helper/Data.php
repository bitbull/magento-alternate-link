<?php
/**
 * Class     Data.php
 * @category Bitbull
 * @package  Bitbull_AlternateLink
 * @author   Mirko Cesaro <mirko.cesaro@bitbull.it>
 */

class Bitbull_AlternateLink_Helper_Data extends Mage_Core_Helper_Abstract{

    public function rewrittenProductUrl($productId, $categoryId, $storeId)
    {
        $coreUrl = Mage::getModel('core/url_rewrite');
        //create the path of the product
        $idPath = sprintf('product/%d', $productId);
        if ($categoryId) {
            //if there is a category it will be added to eht idpath
            $idPath = sprintf('%s/%d', $idPath, $categoryId);
        }
        //set the store url to obtain the rewrite for only the store setted
        $coreUrl->setStoreId($storeId);
        $coreUrl->loadByIdPath($idPath);
        return $coreUrl->getRequestPath();
    }

    function removeVar($url, $varToRemove) {
        return preg_replace('/([?&])'.$varToRemove.'=[^&]+(&|$)/','$1',$url);
    }

    public function getRelParamsOfLinkInHead($url, $headBlock = null){
        if(!$headBlock ){
            $headBlock = Mage::app()->getLayout()->getBlock('head');
        }
        $items = $headBlock->getData('items');

        if(isset($items['link_rel/'.$url]) && isset($items['link_rel/'.$url]['params'])){
            $params = $items['link_rel/'.$url]['params'];
            return trim(str_replace('rel=', '',$params),'"');

        }
        return '';

    }

} 